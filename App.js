import React, { Component } from "react";
import {
  ActivityIndicator,
  ScrollView,
  View,
  StyleSheet,
  Text
} from "react-native";
import { List, ListItem, Header } from "react-native-elements";

export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      results: []
    };
  }

  getData = _ => {
    var API_URL = "https://randomuser.me/api/?results=30&nat=tr";
    fetch(API_URL)
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            results: responseJson.results
          },
          function() {}
        );
      })
      .then(console.log(this.state.results))
      .catch(error => {
        console.error(error);
      });
  };

  componentDidMount() {
    this.getData();
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <ScrollView>
        <Header
          outerContainerStyles={{ backgroundColor: "#f96d00" }}
          leftComponent={{ icon: "menu", color: "#fff" }}
          centerComponent={{ text: "Mail Kutunuz", style: { color: "#fff" } }}
          rightComponent={{ icon: "home", color: "#fff" }}
        />
        <List containerStyle={{ marginBottom: 20 }}>
          {this.state.results.map(l => (
            <ListItem
              roundAvatar
              avatar={l.picture.thumbnail}
              key={l.name.first}
              title={
                l.name.first.toUpperCase() + " " + l.name.last.toUpperCase()
              }
              subtitle={l.email}
              badge={{
                value: 24,
                textStyle: { color: "white" }
              }}
            />
          ))}
        </List>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  //TODO
  title: {}
});

